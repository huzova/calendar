var mysql = require('mysql')
global.con = null

// db conn
var con = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "root", 
    database: "calendar"
});

con.connect((err,db)=>{
    if(err){
        console.log(err, 'cant connect to db')
        return false
    }
    global.con = con
    console.log('connceted to db')
    return true
})