exports.saveEvent = (event, callback) => {
    var sql = "INSERT INTO `calendar`.`events` (`user_id`, `event_desc`) VALUES (" + event.userId + "," + event.eventDesc + ");";

    global.con.query(sql, (err, result) => {
        if (err) 
            return callback (true, err)

        console.log("1 record inserted, ID: " + result.insertId);
        return callback (false, "OK")
    });
}

exports.getEvents = (userId, callback) => {
    var sql = "select * from events where user_id =" + userId

    global.con.query(sql, (err, result) => {
        if (err) 
            return callback (true, err)

        console.log("result", result);
        return callback (false, result)
    });
}

exports.updateEvent = (event, callback) => {
    var sql = "update calendar.events set event_desc='"+ event.eventDesc + "WHERE event_id ="+ event.eventId +";"
    
    global.con.query(sql, (err, result) => {
        if (err) 
            return callback (true, err)

        console.log("result", result);
        return callback (false, "OK")
    });
}
  
exports.deleteEvent = (eventId, callback) => {
    var sql = "delete from events where event_id="+ eventId +";"

    global.con.query(sql, (err, result) => {
        if (err) 
            return callback (true, err)

        console.log("result", result);
        return callback (false, "OK")
    });
}