// server 
var express = require('express')
var app = express()
app.use (express.static('public'))

var cors = require("cors");
app.use(cors());

// to use post 
var formidable = require ('express-formidable')
app.use(formidable())

// db
var conn = require(__dirname+'/db/db_conn.js')
var events = require(__dirname+'/db/events.js')
var users = require(__dirname+'/db/users.js')

// server 
app.listen(3333, (err)=>{
    if (err){
        console.log("can't listen to port 3333")
        return false
    }
    console.log('server listening to port 3333')
})


// save event
app.post('/save-event', (req, res)=>{
    var event = { 
        "eventDesc": req.fields.eventDesc, 
        "dateTime":req.fields.dateTime,
        "userId": req.fields.userId 
    }

    events.saveEvent( event, ( err , result ) => {
        if( err ){
          console.log( result )
          res.send('<html><body>ERROR</body></html>')
          return
        }
        console.log( result )
        res.send('<html><body>OK</body></html>')
        return
    })
})

// get event 
app.get('/get-events', (req, res)=>{
    var userId = req.query.userId

    events.getEvents(userId, ( err , result ) => {
        if( err ){
          console.log( result )
          res.send('<html><body>ERROR</body></html>')
          return
        }
        console.log( result )
        res.send(result)
    })
})

// update event 
app.post('/update-event', (req, res)=>{
    var event = {"eventId": req.fields.eventId, "eventDesc": req.fields.eventDesc, "dateTime": req.fields.dateTime};
    
    events.updateEvent(event, ( err , result ) => {
        if( err ){
          console.log( result )
          res.send('<html><body>ERROR</body></html>')
          return
        }
        console.log( result )
        res.send('<html><body>OK</body></html>')
        return
    })
})

// delete event 
app.post('/delete-event', (req, res)=>{
    var eventId = req.fields.eventId
    console.log("eventId ", eventId);

    events.deleteEvent(eventId, ( err , result ) => {
        if( err ){
          console.log( result )
          res.send('<html><body>ERROR</body></html>')
          return
        }
        console.log( result )
        res.send('<html><body>OK</body></html>')
        return
    })
})


// update profile
app.post('/update-profile', (req, res)=>{
    // console.log("req ", req.body);
    var user = {"mail":req.fields.mail, "password": req.fields.password, "userId": req.fields.userId}
    console.log("user ", user);
    if (
            user.mail == undefined || 
            user.password == undefined || 
            user.userId == undefined
        ){
            res.send('ERROR 1')
            return
        }

    users.updateProfile(user, ( err , result ) => {
        if( err ){
          console.log( err )
          res.send('ERROR 2')
          return
        }
        // console.log( result )
        res.send(result)
        return
    })
})

// delete profile
app.post('/delete-profile', (req, res)=>{
    var userId = req.fields.userId

    users.deleteProfile(userId, ( err , result ) => {
        if( err ){
          console.log( result )
          res.send('<html><body>ERROR</body></html>')
          return
        }
        console.log( result )
        res.send('<html><body>OK</body></html>')
        return
    })
})

// get user by id 
app.get('/get-profile', (req, res)=>{
    var userId = req.query.userId

    users.getProfile(userId, ( err , result ) => {
        if( err ){
          console.log( result )
          res.send('<html><body>ERROR</body></html>')
          return
        }
        console.log( result )
        res.send('<html><body>OK</body></html>')
        return
    })
})

// sign up
app.post('/signup', (req, res)=>{
    var user = {"mail":req.fields.mail, "password": req.fields.password }
    
    users.signup(user, ( err , result ) => {
        if( err ){
          console.log( result )
          res.send('<html><body>ERROR</body></html>')
          return
        }
        console.log( result )
        res.send('<html><body>insert Id: ' + result.insertId + '</body></html>')
        return
    })
})

app.post('/login', (req, res) => {
    var user = {"mail":req.fields.mail, "password": req.fields.password }
    users.login(user, ( err, result ) => {
        if (err){
            console.log( result )
            res.send('ERROR')
            return
        }   
            console.log( result )
            res.send(result[0])
            return
    })

})