const host = 'http://localhost:3333/';

export const onLogout = props => ({
    type: 'ONLOGOUT'
})

export const fetchEvents = (userId) => {
    return (dispatch) => {
        console.log('call api')
        return fetch(host + 'get-events?userId=' + userId)
            .then(response => response.text())
            .then(json => dispatch(
                { type: "FetchEvents", data: JSON.parse(json) }))
            .catch(err => dispatch(
                { type: "ErrFetchEvents",msg: "No events found" }))
    }
}

export const deleteEvent = (eventId) => {
    let json = {eventId: eventId}
    return (dispatch) => {
        console.log('call api eventId', eventId)
        return fetch(host + 'delete-event', {
            method: 'POST',  
            body: JSON.stringify(json),  
            headers:{
              'Content-Type': 'application/json'
             }
              })
            .then(response => dispatch(
                { type: "DeleteEvent", id: eventId }))
            .catch(err => dispatch(
                { type: "ErrDeleteEvent",msg: "No events found" }))
    }
}

export const fetchUser = (loginFrm) => {
    return (dispatch) => {
        console.log('call login api')
        return fetch(host + 'login', {
            method: 'POST',  
            body:loginFrm,  
            headers:{
              'Content-Type': 'application/json'
             }
              })
            .then(
                response => response.json())
            .then(json => dispatch(
                { type: "FetchUser", user: json }))
            .catch(err => dispatch(
                { type: "ErrFetchUser",msg: "Unable to fetch user" }))
    }

}

export const updateUser = (updateUserFrm) => {
    return (dispatch) => {
        console.log('call update user api')
        return fetch(host + 'update-profile', {
            method: 'POST',  
            body: updateUserFrm,  
            headers:{
              'Content-Type': 'application/json'
             }
              })
            .then(
                response => response.json(),
                error => console.log('An error occurred.', error)
            )
            .then(response => dispatch(
                { type: "UpdateUser", user: response }))
            .catch(err => dispatch(
                { type: "ErrUpdateUser",msg: "Unable to update user" }))
    }
}

export const deleteUser = (userId) => {
    let json = {userId: userId}
    return (dispatch) => {
        console.log('call login api')
        return fetch(host + 'delete-profile', {
            method: 'POST',  
            body: JSON.stringify(json),  
            headers:{
              'Content-Type': 'application/json'
             }
              })
            .then(
                response => response.text())
            .then( dispatch({ type: "ONLOGOUT" }))
            .catch(err => dispatch(
                { type: "ErrDeleteUser",msg: "Unable to delete user" }))
    }
}