const userState = (state = {}, action) => {
 console.log("action ", action);

  switch (action.type) {
    case 'ONLOGOUT':
      return { ...state, user: null }

    case "FetchUser":
      return { ...state, user: action.user }
    case "ErrFetchUser":
      return { ...state, ErrFetchUser: action.msg }

    case "UpdateUser":
      return { ...state, user: action.user }
    case "ErrUpdateUser":
      return { ...state, ErrUpdateUser: action.msg }

    case "ErrDeleteUser":
      return { ...state, ErrDeleteUser: action.msg }

    case "FetchEvents":
      return { ...state, data: action.data }
    case "ErrFetchEvents":
      return { ...state, ErrFetchEvents: action.msg }
    
    case "DeleteEvent":
        const newData = state.data.filter((i) => {
          return i.event_id !== action.id
        });
        const newState = { ...state, data: newData };
        return newState
    case "ErrDeleteEvent":
      return { ...state, ErrDeleteEvent: action.msg }

    default:
      return state
  }
}

export default userState