import React from 'react';
import { connect } from 'react-redux';
import { fetchUser } from '../js/actions';
import { Redirect } from 'react-router-dom';
import TopBar from '../components/topBar';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form'


const Login = (props) => {
  let mail, password = null;
  
  const onSubmitFrm = e => {
    e.preventDefault()
    
    mail = mail.value;
    password = password.value;
    
    const data = { mail, password }
    console.log("data ", data);
    const newData = JSON.stringify(data)
    
    props.dispatch(fetchUser(newData))
  }

  return (
    <div>
      <TopBar title = "Login" />
      <div className="flex-wrap-center">

          <Form id="loginFrm" onSubmit = { onSubmitFrm } >
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control ref={(input) => { mail = input; }} name="mail" type="email" placeholder="Enter email" defaultValue="c@c"/>
              <Form.Text className="text-muted">
                We'll never share your email with anyone else.
              </Form.Text>
            </Form.Group>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email address</Form.Label>
              <Form.Control ref={(input) => { password = input; }} name="password" type="password" placeholder="Enter email" defaultValue="c"/>
            </Form.Group>
            <Button type="submit" variant="primary">
              Login
            </Button>
            { 
              props.state.ErrFetchUser &&
              <p>Try again !</p>
            }
          </Form>
          
        {
          props.state.user &&
          <Redirect to="/notes" push={true} />
        }
      </div>
    </div>
  )
}


const selectState = (state) => {
  console.log("state-login", state);
  return {
    state: state
  }
};

export default connect(selectState)(Login)