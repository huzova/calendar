import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchEvents } from '../js/actions';
import NotesItem from './notesItem';
import TopBar from '../components/topBar';
import Button from 'react-bootstrap/Button';



class Notes extends Component {

  componentDidMount() {
    if (userId !== undefined)
      this.props.onFetchEvents()
  }

  render() {
    return (
      <main className="notes">
        <TopBar title="Notes"/>
        {/* {this.props.error && <p>{this.props.error}</p>} */}
        {
          this.props.data &&
          <section className="notes__section">
            {this.props.data.map((event) => (
                <NotesItem key={event.event_id} event_id={event.event_id} desc={event.event_desc}/>
            ))}
          </section>
        }
        {
          this.props.data && this.props.data.length === 0 && 
          <p>No notes yet! Create one :)</p>
        }
        {
          !this.props.data && 
          <p>Please login first :)</p>
        }
        {
          // this.props.data &&
          // <Button type="button">add note</Button>
        }
      </main>
    );
  }
}

let userId;
const mapStatetoProps = (state) => {
  console.log("state-Notes", state);
  if(state.user) 
    userId = state.user.user_id;
  return { num: state.num, data: state.data, error: state.error, user: state.user }
}

const mapDispatchprops = (dispatch) => {
  if (userId === undefined)
    return {}

  return { onFetchEvents: () => dispatch(fetchEvents(userId)) }
}

export default connect(mapStatetoProps, mapDispatchprops)(Notes);