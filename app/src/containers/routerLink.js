import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { onLogout } from '../js/actions';
import Nav from 'react-bootstrap/Nav'

const RouterLink = (props, { dispatch }) => (
    <nav className="navigation">
        { 
            !props.user &&
            <Nav.Item>
                <Link to={'login'} className="navigation__link">Login</Link>
            </Nav.Item>
        }
        {
            props.user &&
            <Nav>
                <Nav.Item>
                    <Link to={''} className="navigation__link">Notes</Link>
                </Nav.Item>
                <Nav.Item>
                    <Link to={'profile'} className="navigation__link">Profile</Link>
                </Nav.Item>
                <Nav.Item onClick={() => props.dispatch(onLogout())}>
                    <Link to={'login'} className="navigation__link">Logout</Link>
                </Nav.Item>
            </Nav>
        }
    </nav>
)

const select = (state) => {
    // console.log("state-router-link", state);
    return {
        user: state.user
    }
};

export default connect(select) (RouterLink);