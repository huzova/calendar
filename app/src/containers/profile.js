import React from 'react';
import { connect } from 'react-redux';
import { updateUser, deleteUser } from '../js/actions';
import TopBar from '../components/topBar';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

const Profile = (props) => {

    const onSubmitFrm = (e) => {
        e.preventDefault();

        let formData = new FormData(e.target);
        formData.append("userId", props.user.user_id);

        var newFormData = {};
        formData.forEach((value, key) => {newFormData[key] = value});

        if (newFormData.mail !== props.user.mail || 
            newFormData.password !== props.user.password) {
                var json = JSON.stringify(newFormData);
                return props.dispatch(updateUser(json));
        }
    }

    const onDelete = () => {
        console.log("del user ", props.user.user_id);
        return props.dispatch(deleteUser(props.user.user_id));
    }

    return (
        <div>
            <TopBar title = "Profile" />
            { props.user &&
                <div className="flex-wrap-center">
                    <div>
                        <Form onSubmit={onSubmitFrm}>
                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control name="mail" type="email" defaultValue={props.user.mail}/>
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>
                        <Form.Group controlId="formBasicPassword">
                            <Form.Label>Password</Form.Label>
                            <Form.Control name="password" type="password" defaultValue={props.user.password}/>
                        </Form.Group>

                            {/* <label>password</label>
                            <input type="tetx" name="password" defaultValue={props.user.password}/> */}
                            <Button type="submit">Update profile</Button>
                        </Form>
                        <Button onClick={onDelete} variant="link">Delete profile</Button>
                    </div>
                </div>
            }
            {
                !props.user && 
                <span className="flex-wrap-center">Please login first :)</span>
            }
        </div>
    )
}

const mapStatetoProps = (state) => {
    console.log("state-profile", state);
    return {
        user: state.user
    }
};


export default connect(mapStatetoProps)(Profile)