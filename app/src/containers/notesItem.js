import React from 'react';
import { connect } from 'react-redux';
import Alert from 'react-bootstrap/Alert';
import Button from 'react-bootstrap/Button';
import { deleteEvent } from '../js/actions';


  const NotesItem = (props) => {

    const onDelete = (e) => {
      e.preventDefault();
      return props.dispatch(deleteEvent(props.event_id));
    }

      return (
        <div className="notesItem">
          <Alert className="notesItem__alert" key={props.event_id} variant="primary">
            {props.desc + " - " + props.event_id }
          </Alert>
          <Button variant="link" onClick={(e) => onDelete(e)} >Delete</Button>
        </div>
      )
}

const mapStatetoProps = (state) => {
  console.log("state-itemNotes", state);
  return {
      data: state.data
  }
};

export default connect(mapStatetoProps) (NotesItem);