import React, { Component } from 'react';
import 'whatwg-fetch';
import Router from '../router';

class App extends Component {
  render(){
    return(
      <div>
        <Router/>
      </div>
    )
  }
}

export default App;