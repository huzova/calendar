import React from 'react';

// this is a top bar component 
// it is dispalyed at the top of the page
const TopBar = props => (
    <h1 className="topBar__title">{props.title}</h1>
);

export default TopBar;