import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Notes from '../containers/notes';
import Login from '../containers/login';
import Profile from '../containers/profile';
import RouterLink from '../containers/routerLink';

const Router = props => (
    <div>
        <RouterLink/>
        <Switch>
            <Route exact path='/' component={Login}/>
            <Route exact path='/notes' component={Notes}/>
            <Route exact path='/login' component={Login}/>
            <Route exact path='/profile' component={Profile}/>
        </Switch>
    </div>
)

export default Router;